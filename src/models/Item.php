<?php

namespace mywishlist\models;

//class Item
class Item extends \Illuminate\Database\Eloquent\Model{
	//nom de la table dans phpMyAdmin
	protected $table ='item';
	//nom de la clef primaire dans la table item
	protected $primaryKey = 'id';
	//colonne update et create ne sont pas automatiquement gérées
	public $timestamps = false;


	//function liste qui associe un item a une liste
	public function liste() {
		return $this->hasMany('mywishlist\models\Liste', 'no');
	}

	
}

