<?php

namespace mywishlist\models;

class User extends \Illuminate\Database\Eloquent\Model{
	//nom de la table dans phpMyAdmin
	protected $table ='user';
	//nom de la clef primaire dans la table item
	protected $primaryKey = 'user_id';
	//colonne update et create ne sont pas automatiquement gérées
	public $timestamps = false;
	
	//function getByEmail qui permet d avoir en fonction du parametre de l email, le user adapté
	public static function getByEmail($mail){
        $email = filter_var($mail,FILTER_SANITIZE_EMAIL);
        return User::where('email','=',$mail)->first();
    }
    //function getById qui permet d avoir en fonction du parametre de l id, le user adapté
	public static function getById($id){
        return User::where('user_id','=',$id)->first();
    }
}