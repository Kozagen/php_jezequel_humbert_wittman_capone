<?php

namespace mywishlist\models;
//class Item
class Liste extends \Illuminate\Database\Eloquent\Model{
	//nom de la table dans phpMyAdmin
	protected $table ='liste';
	//nom de la clef primaire dans la table item
	protected $primaryKey = 'no';
	//colonne update et create ne sont pas automatiquement gérées
	public $timestamps = false;

	//function item qui associe une liste pour plusieurs items
	public function item() {
		return $this->belongsTo('mywishlist\models\Item', 'liste_id');
	}

}