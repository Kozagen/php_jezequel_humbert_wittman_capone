<?php

namespace mywishlist\controleur;
use mywishlist\vue\VueNavigation;
class Authentification{
	
	public static function createUser($email, $password,$nom,$prenom){
		//variable error qui passe a vrai si il y a une erreur et empeche l'inscription de l'utilisateur
		$error=false;
		//filtration du password
		if($password == filter_var($password, FILTER_SANITIZE_STRING)){
			$password = filter_var($password, FILTER_SANITIZE_STRING);
		} 
		else {
		$error=true;
		}
		//filtration de l'email
		if($email==filter_var($email, FILTER_VALIDATE_EMAIL)){
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		}
		else{
			$error=true;
		}		
		//filtration du nom de la personne  
		if($nom == filter_var($nom, FILTER_SANITIZE_STRING)){
			$nom = filter_var($nom, FILTER_SANITIZE_STRING);
		}
		else{
			$error=true;
		}
		//filtration du prenom
		if($prenom == filter_var($prenom, FILTER_SANITIZE_STRING)){
			 $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
		}
		else{
			$error=true;
		}
		
		//si il n'y a pas d'erreur inscription reussie
		if (!$error){
			//securité en hashant le pasword et creation de l'utilisateur
            $password = password_hash($password, PASSWORD_DEFAULT, Array('cost' => 12));
            $u = new \mywishlist\models\User();
            $u->email = $email;
            $u->password = $password;
            $u->nom = $nom;
			$u->prenom = $prenom;
            $u->save();
		}
		
	}

	// methode qui permet d'athentifier une personne a partir de l'email et du password mis en parametre
    public static function authenticate($email, $pass) {
            $app = \Slim\Slim::getInstance();
            $v = new VueNavigation();
            $user = \mywishlist\models\User::getByEmail($email);
            //verifie si le user existe bien 
            if ($user != null) {
                $p = $user->password;
                if (password_verify($pass, $p)) {
                    $_SESSION['id'] = $user->user_id;
                    $_SESSION['email'] = $email;
                    $app->redirect($app->urlFor("accueil"));
                } else {
                    print $v->render("errC");
                }
            } else {
                print $v->render("errC");
            }
        }

}