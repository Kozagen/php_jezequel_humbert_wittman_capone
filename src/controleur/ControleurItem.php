<?php

namespace mywishlist\controleur;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\vue\VueNavigation;
use Slim\Slim;

class ControleurItem{

  //methode qui permet de supprimer l'idem en fonction de son id mis en parametre
	public static function supprimeritem($id){
        $item = \mywishlist\models\Item::where('id','=', $id);
        if (isset($_SESSION['id'])) {
            $item->delete();
         }
    }
      
      //methode qui permet de modifier l'item en fonction de son id mis en parametre 
      public static function modifierItem($id) {
        $i = Item::where('id', '=', $id)->first();
        //verif que nom non null
        if ($_POST['nom'] != ""){
          //filtre le nom
            if ($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)) {
            $i->nom=$_POST['nom'];
            $i->save();
            }
         }
        //verif que descritpion non null
        if ($_POST['dsc'] != ""){
          //filtre descritpion
            if ($_POST['desc'] == filter_var($_POST['desc'], FILTER_SANITIZE_STRING)) {
            $i->description=$_POST['desc'];
            $i->save();
            }
         }
         //verif que tarif non null
        if ($_POST['tarif'] != ""){
          //filtre tarif
             if ($_POST['tarif'] == filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT)) {
            $i->tarif=$_POST['tarif'];
            $i->save();
            }
         }
        
      //  print (new VueNavigation())->render("modifL");
    }
}