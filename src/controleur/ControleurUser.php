<?php

namespace mywishlist\controleur;
use mywishlist\models\Liste;
use mywishlist\models\User;
use mywishlist\vue\VueNavigation;
use Slim\Slim;

class ControleurUser
{

	//methode index 
    public function index(){
	
        $vue = new VueNavigation();
        print $vue->render("index");
		
    }

    //methode qui pemet la connexion d un utilisateur
    public function connexion() {
    //verifie si l'email et le password existe bien
	if(isset($_POST['email']) && isset ($_POST['password'])){
    //recupere l'email
	$email=$_POST['email'];
    //recupere le password
	$password=$_POST['password'];
	try{
        //applique la methode de l'autehntification 
		Authentification::Authenticate($email,$password);
		//Authentification::loadProfile($email);
		
		}
		catch (AuthException $ae){
			echo"mauvais login name ou password<br>";
		}
	} else {
		$vue = new VueNavigation();
        print $vue->render("connexion");
	    }
    }
    //methode liste 
    public function listes() {
    	$vue = new VueNavigation();
        print $vue->render("listes");
    }
	
	//methode inscription qui permet d 'inscrire si tout les critere sont verifiés
     public function inscription() {
        //verification 
		if(isset($_POST['email']) && isset ($_POST['password'])&& isset($_POST['nom']) && isset($_POST['prenom'])){
            //recupere l'email
			$email=$_POST['email'];
            //recupere le password
			$password=$_POST['password'];
            //recupere le nom
			$nom=$_POST['nom'];
            //recupere le prenom
			$prenom=$_POST['prenom'];
			try{
                //applique la methode de creation dutilisateur en fonction des parametres recuperés
			Authentification::createUser($email, $password, $nom, $prenom);
			$vue = new VueNavigation();
			print $vue->render("inscription");
			}
			catch (AuthException $ae){
			echo"mauvais login name ou password ou nom ou prenom<br>";
			}
		}
		else{
			$vue = new VueNavigation();
        print $vue->render("inscription");
		}
	 }

     //methode de deconnexion
	 public function deconnexion() {
        session_destroy();
        $app = Slim::getInstance();
        $app->redirect($app->urlFor("accueil"));
     }

     //methode qui permet de modifier le profil
	public function modifProfil() {

        if (isset($_SESSION['id'])) {
            $u = User::getById($_SESSION['id']);
            //verifie si l'element est bien completé
            if ($_POST['nom'] != "") {
                //filtre le nom
                if($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)){
                $u->Nom = $_POST['nom'];
                }
            }
            //verifie si l'element est bien completé
            if ($_POST['prenom'] != "") {
                //filtre le prenom
                if($_POST['prenom'] == filter_var($_POST['prenom'], FILTER_SANITIZE_STRING)){
                $u->Prenom = $_POST['prenom'];
                }
            }
            //verifie si l ancien password est le bon
            if (password_verify($_POST['ancienmdp'], $u->password)) {
                //filtre ancienpassword
                if($_POST['ancienmdp'] == filter_var($_POST['ancienmdp'], FILTER_SANITIZE_STRING)){
                    //filtre nouveaupassword1
                    if($_POST['nouveaumdp1'] == filter_var($_POST['nouveaumdp1'], FILTER_SANITIZE_STRING)){
                        //filtre nouveaupassword2
                        if($_POST['nouveaumdp2'] == filter_var($_POST['nouveaumdp2'], FILTER_SANITIZE_STRING)){
                            //verifie si la verification est identique que la creation du password
                            if ($_POST['nouveaumdp1']===$_POST['nouveaumdp2']) {
                                //hash le nouveau password
                            $u->password=password_hash($_POST['nouveaumdp2'], PASSWORD_DEFAULT, Array('cost' => 12));
                            }
                        }
                    }
                }
            }
            //nous avions pensé que save suffisait pour modifier mais cela applique l'ato increment c'est pour ca que nous avons cherché et pris update pour garder le user_id
            $u->update();
        }
        $app = Slim::getInstance();
        $app->redirect($app->urlFor("accueil"));
    }

    public static function supprimerUser() {
        $u=User::getById($_SESSION['id']);
        $u->delete();
        session_destroy();
    }



}
