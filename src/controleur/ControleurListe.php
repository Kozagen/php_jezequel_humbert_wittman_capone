<?php

namespace mywishlist\controleur;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\vue\VueNavigation;
use Slim\Slim;

class ControleurListe{

    //methode qui permet de creer une liste 
	public static function creerliste()
    {
        if (isset($_SESSION['id'])) {
            $n = new Liste(); 
			$n->user_id = $_SESSION['id'];
			$n->save();
			$l=$n->no;
            $app = Slim::getInstance();
            $app->redirect($app->urlFor('modListe', array('id' => $l)));
	    }
    }

    //methode qui permet de modifier une liste en fonction de l'id de la liste mis en parametre
    public static function modifierListe($id) { 
	    $l = Liste::where('no', '=', $id)->first();
        //verifie s'il existe bien un titre
      if ($_POST['title'] != ""){
        //filtre le titre
        if ($_POST['title'] == filter_var($_POST['title'], FILTER_SANITIZE_STRING)) {
           $l->titre=$_POST['title'];

        }
      }
      //verifie s'il existe bien une description 
     if ($_POST['dsc'] != ""){
         //filtre la description
	     if ($_POST['dsc'] == filter_var($_POST['dsc'], FILTER_SANITIZE_STRING)) {
           $l->description=$_POST['dsc'];

        }
      }

      if ($_POST['msg'] != ""){
         //filtre la description
         if ($_POST['msg'] == filter_var($_POST['msg'], FILTER_SANITIZE_STRING)) {
           $l->msg=$_POST['msg'];

        }
      }
        $l->update();
        print (new VueNavigation())->render("modifL");
    }

    //methode qui permet de supprimer la liste en fonction de l id de la liste mis en parametre
	public static function supprimerliste($id){
		$items = \mywishlist\models\Item::where('liste_id','=', $id);
		if (isset($_SESSION['id'])) {
		 	foreach( $items as $item){
		 		$item->delete();
		 	}
		 	$l=liste::where('no','=',$id);
		 	$l->delete();
		 }
	}


    //methode qui permet d ajouter un item a la liste en fonction de l'id de la liste pour l item
	public static function ajouterItem($id)
    {
        //verification
        if (isset($_SESSION['id'])) {
            //verif si nom desc tarif sont pas null
            if ($_POST['nom'] != "" && $_POST['desc'] != "" && $_POST['tarif'] != "") {
                //filtre le nom
                if ($_POST['nom'] == filter_var($_POST['nom'], FILTER_SANITIZE_STRING)) {
                    //filtre la description
                    if ($_POST['desc'] == filter_var($_POST['descr'], FILTER_SANITIZE_STRING)) {
                        //filtre le tarif
                        if ($_POST['tarif'] == filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT)) {
                            $i = new Item();
                            $i->nom = $_POST['nom'];
                            $i->descr = $_POST['desc'];
                            $i->tarif = $_POST['tarif'];
                            $i->liste_id = $id;
                        }
                    }
                }
            }
            //verif que l url n est pas nul
            if ($_POST['url'] != "") {
                //filtre l url
                if ($_POST['url'] == filter_var($_POST['url'], FILTER_SANITIZE_URL)) {
                    $i->url = $_POST['url'];
                }
            }
            $i->save();
        }
    }

    public function  retirerItem() {

    }
}