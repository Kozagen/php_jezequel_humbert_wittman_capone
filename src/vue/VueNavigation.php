<?php

namespace mywishlist\vue;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\User;
use Slim\Slim;

/**
*
*/
class VueNavigation
{
	private $objet;

    public function render($select) {
    	switch ($select) {
    		case "index":
    			$content = $this->index();
    			break;

    		case "connexion":
    			$content = $this->connexion();
    			break;

            case "listes":
                $content = $this->listes();
                break;
            case "listesPub" :
                $content = $this->listesPub();
                break;
            case "inscription":
                $content = $this->inscription();
                break;
            case "errC":
                $content = $this->connexion()."<div class='row'><div class='col s12 m6 l4 offset-m3 offset-l4'><p class=\"card-panel red accent-3 z-depth-5 center\" id=\"dsc\">Erreur : email ou mot de passe incorrect</p></div></div>";
                break;
            case "profile":
                $content = $this->profile();
                break;
            case "modifL" :
                $content = $this->modifListe();
                break;
            case "modifI" :
                $content = $this->modifItem();
                break;
            case "affItem" :
                $content = $this->affItem();
                break;
            case "denied" :
                $content = "<div class='row'><div class='col s12 m6 l4 offset-m3 offset-l4'><p class=\"card-panel red accent-3 z-depth-5 center\" id=\"dsc\">Access Denied</p></div></div>";
                break;
            case "affListeToken" :
                $content=$this->affListeToken();
                break;
    		default:
    			$content = "Mauvais affichage";
    			break;
    	}
    	return VueHTML::getHeaders().VueHTML::getMenu().$content.VueHTML::getFooter();
    }

    private function index() {
    	$r="<div class='container'><p class=\"card-panel grey lighten-2\" id=\"dsc\">
            Bienvenue sur My Whishlist, un site qui vous permet de créer et personnaliser votre liste de souhait ! Que ce soit pour une occasion spéciale, un mariage, un baptême, une naissance ou encore un anniversaire, faîtes votre liste, partagez là vos amis !
    	</p></div>
      ";
    	if (!isset($_SESSION['id'])) {
    	    $r.=$this->connexion();
        }
        return $r;
    }

    private function connexion() {
        $app = \Slim\Slim::getInstance();
        $r_inscription = $app->urlFor("inscription");
        $r_co = $app->urlFor("connexionP");
    	return
    	"<br><div class=\"row center-align\">
                    <div class='card-panel grey lighten-2 col s12 m6 l4 offset-l4 offset-m3'>
                        <form method='POST' class=\"col s12\" action='$r_co'>
                          <div class=\"row\">
                            <div class=\"input-field\">
                              <input name='email' id=\"email\" type=\"email\" class=\"validate\">
                              <label for=\"email\">Email</label>
                            </div>
                          </div>
                          <div class=\"row\">
                            <div class=\"input-field\">
                              <input name='password' id=\"password\" type=\"password\" class=\"validate\">
                              <label for=\"password\">Password</label>
                            </div>
                          </div>
                          <div class = \"row\">
                            <input type=\"submit\"class=\"waves-effect waves-light btn indigo darken-1\" value='Connexion'/>
                            <a class=\"waves-effect waves-light btn grey\" id=\"wish\"href=\"$r_inscription\">Pas encore inscrit ?</a>
                          </div>
                        </form>
                    </div>
              </div>";
    }

	private function inscription(){
        $app = \Slim\Slim::getInstance();
        $r_ins = $app->urlFor("inscriptionP");
		return
    	"<br><div class=\"row\">
                <div class='col s12 m6 l4 offset-l4 offset-m3 card-panel grey lighten-2'>
                    <form method=\"POST\" class=\"\" action=\"".$r_ins."\">
                      <div class=\"row\">
                        <div class=\"input-field col s6\">
                          <input name='nom' id=\"first_name\" type=\"text\" class=\"validate\">
                          <label for=\"first_name\">Nom</label>
                        </div>
                        <div class=\"input-field col s6\">
                          <input name='prenom' id=\"last_name\" type=\"text\" class=\"validate\">
                          <label for=\"last_name\">Prenom</label>
                        </div>
                      </div>
                      <div class=\"row\">
                        <div class=\"input-field\">
                          <input name='email' id=\"email\" type=\"email\" class=\"validate\">
                          <label for=\"email\">Email</label>
                        </div>
                      </div>
                      <div class=\"row\">
                        <div class=\"input-field\">
                          <input name='password' id=\"password\" type=\"password\" class=\"validate\">
                          <label for=\"password\">Password</label>
                        </div>
                      </div>
                      <div class = \"row center-align\">
                        <input type=\"submit\" class=\"waves-effect waves-light btn indigo darken-1\" value='Inscription'/>
                      </div>
                    </form>
                </div>
          </div>";
	}

	private function profile() {
        $app = \Slim\Slim::getInstance();
        $r_pro = $app->urlFor("mProfil");
        $remove = $app->urlFor("suppUser");
        $uti = \mywishlist\models\User::getById($_SESSION['id']);
      //  $test=$_SESSION
        $html = "<br><div class=\"row center-align\">
        <div class=\"col l6 offset-l3\">
          <div class=\"card grey lighten-2\">
            <div class=\"card-content\">
              <span class=\"card-title\">Profil de ".$uti->Prenom." ".$uti->Nom."</span>
              <form method=\"POST\" class=\"row\" action=\"$r_pro\">
                <div class=\"input-field col s5 offset-s1\">
                  <input name='nom' id=\"nom\" type=\"text\" value=\"";
                  $html.=$uti->Nom;
                  $html.="\">
                  <label for=\"nom\">Nom</label>
                </div>
                <div class=\"input-field col s5\">
                  <input name='prenom' id=\"prenom\" type=\"text\"  value=\"$uti->Prenom\">
                  <label for=\"prenom\">Prenom</label>
                </div>
                <div class=\"input-field col s10 offset-s1\">
                  <input name='ancienmdp' id=\"mdpA\" type=\"password\">
                  <label for=\"mdpA\">Mot de passe actuel</label>
                </div>
                <div class=\"input-field col s5 offset-s1\">
                  <input name='nouveaumdp1' id=\"passN1\" type=\"password\" >
                  <label for=\"passN1\">Nouveau mot de passe</label>
                </div>
                <div class=\"input-field col s5\">
                  <input name='nouveaumdp2' id=\"passN2\" type=\"password\">
                  <label for=\"passN2\">Confirmation mot de passe</label>
                </div>
                  <input class='btn waves-effect indigo darken-1' type='submit' value='Enregistrer profil' /><br>
                  <br><a class='btn waves-effect red' href='$remove'>Supprimer sont profil</a>
              </form>
            </div>
          </div>
        </div>
      </div>";
      return $html;
    }

    private function listes() {
      //$r = "<ul class=\"collapsible popout\" data-collapsible=\"accordion\">";
      $r=(new VueParticipant())->listeSouhaits();
      //$r = $r."</ul>";
      return $r;
    }

    private function listesPub() {
        return (new VueParticipant())->listePublique();
    }

    private function modifListe() {
        $app = \Slim\Slim::getInstance();
        $e=explode('/', $app->request->getResourceUri());
        $url=$app->urlFor('modListePost', array('id' => $e[2]));
        $supp=$app->urlFor('suppListe', array('id' => $e[2]));
        $tok=$app->urlFor('addUrl', array('id' => $e[2]));
        $li = Liste::where('no', '=', $e[2])->first();
        $r = "
<div class=\"row\">
    <div class=\"col s12 m6 offset-m3\">
      <div class=\"card grey lighten-2 center-align\">
        <form method=\"POST\" action=\"$url\">
          <div class=\"input-field col l4 offset-l3\">
            <input name=\"title\" value=\"$li->titre\" id=\"title\" type=\"text\">
            <label class=\"active\" for=\"title\">Nom de la liste</label>
          </div>
          <div class=\"input-field col l2\">
            <input type=\"text\" class=\"datepicker\">
            <label class=\"active\" for=\"date\">Échéance</label>
          </div>
          <div class=\"input-field col s12\">
            <input name=\"dsc\" id=\"desc\" type=\"text\" value=\"$li->description\">
            <label class=\"active\" for=\"desc\">Description</label>
          </div>
          <div class=\"input-field col s12\">
            <input name=\"msg\" id=\"msg\" type=\"text\" value=\"$li->msg\">
            <label class=\"active\" for=\"msg\">Message</label>
          </div>
          <div class='input-field col s12'>";
        if ($li->token != "") {
            $r.=$_SERVER['SERVER_NAME'].$app->request->getRootUri()."/listeUrl/$li->token</div>";
        } else {
            $r.="<a class=\"btn indigo darken-1\" href='$tok'>Génerer un lien</a>
          </div>";
        }
           $r.="<div class=\"fixed-action-btn horizontal\">
            <a class=\"btn-floating btn-large light-blue darken-3\">
              <i class=\"large material-icons\">mode_edit</i>
            </a>
            <ul>
              <li><a class=\"btn-floating tooltipped black\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Supprimer la liste\" href='$supp'><i class=\"material-icons\">clear</i></a></li>
              <li><a class=\"btn-floating tooltipped disabled red\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Supprimer la selection (incomplet)\"><i class=\"material-icons\">delete</i></a></li>
              <li><button class=\"btn-floating tooltipped green\" type=\"submit\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Sauvegarder\"><i class=\"material-icons\">done</i></button></li>
              
            </ul>
          </div> 
        </form>
          <table class=\"highlight centered responsive-table\">
        <thead>
          <tr>
              <th>Action</th>
              <th>Nom</th>
              <th>Description</th>
              <th>Prix</th>
          </tr>
        </thead>

        <tbody>";
        $lItem = Item::where('liste_id', '=', $e[2])->get();
        foreach ($lItem as $item) {
            $suppItem = $app->urlFor('suppItem', array('id' => $item->id));
            $modItem = $app->urlFor('modItem', array('idL' => $e[2], 'id' => $item->id));
            $urlItem = $app->urlFor('affItem', array('id' => $item->id));
            $r=$r."<tr>
            <td>
                <ul>
                  <li><a href='$modItem' class=\"btn light-blue darken-3\"><i class=\"material-icons\">edit</i></a></li>
                  <li><a href='$suppItem' class=\"btn black\"><i class=\"material-icons\">delete</i></a></li>
                </ul>
            </td>
            <td><a href='$urlItem' class='black-text'>$item->nom</a></td>
            <td>$item->descr</td>
            <td>$item->tarif</td>
          </tr>";
        }
         /* <tr>
            <td><input type=\"checkbox\" id=\"test7\"/><label for=\"test7\"></label></td>
            <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
          </tr>*/
        $add=$app->urlFor('addItem', array('id' => $e[2]));
        $r=$r."</tbody>
      </table>
        <a class=\"btn-floating tooltipped fab btn-large waves-effect waves-light green\" href='$add' data-position=\"top\" data-delay=\"50\" data-tooltip=\"Ajouter un item\"><i class=\"material-icons\">add</i></a>  
      </div>
    </div>
  </div>";
        return $r;
    }

    private function modifItem() {
        $app = Slim::getInstance();
        $e=explode('/', $app->request->getResourceUri());
        $vaD = "";
        $vaP = "";
        $vaT = "";
        $vaU = "";
        if (isset($e[3])) {
            $url = $app->urlFor('modItemP', array('idL' => $e[2],'id' => $e[3]));
            $i=Item::where('id', '=', $e[3])->first();
            $vaT = $i->nom;
            $vaD = $i->descr;
            $vaP = $i->tarif;
            $vaU = $i->url;
            $aff = "Modifier";
        } else {
            $url = $app->urlFor('addItemP', array('id' => $e[2]));
            $aff = "Créer et ajouter";
        }
        return "<div class=\"row\">
        <div class=\"col s12 m8 offset-m2 l6 offset-l3\">
        <div class=\"card grey lighten-2\">
            <form method=\"post\" action='$url' enctype=\"multipart/form-data\">
                <div class=\"row\">
                    <div class=\"input-field col l6 offset-l2\">
                        <input class=\"validate\" name=\"title\" value=\"$vaT\" id=\"title\" type=\"text\" required>
                        <label class=\"active\" for=\"title\">Nom de l'item</label>
                    </div>
                    <div class=\"input-field col l2\">
                        <input class=\"validate\" name=\"prix\" value=\"$vaP\" id=\"prix\" type=\"text\" required>
                        <label class=\"active\" for=\"prix\">Prix</label>
                    </div>
                    <div class=\"input-field col s12\">
                        <input class=\"validate\" name=\"dsc\" id=\"desc\" type=\"text\" value=\"$vaD\" required>
                        <label class=\"active\" for=\"desc\">Description</label>
                    </div>
                    <div class=\"input-field col l6\">
                        <input name=\"url\" id=\"url\" type=\"url\" value=\"$vaU\">
                        <label class=\"active\" for=\"url\">Url (optionnel)</label>
                    </div>
                    <div class=\"file-field input-field col l6\">
                          <div class=\"btn indigo darken-1\">
                            <span>File</span>
                            <input type=\"file\" name='file'>
                          </div>
                          <div class=\"file-path-wrapper\">
                            <input class=\"file-path validate\" type=\"text\">
                          </div>
                        </div>
                </div>
                <div class=\"center\">
                    <button class=\"btn waves-effect waves-light indigo darken-1\" type=\"submit\">$aff</button>
                </div><br>
            </form>
        </div>
      </div>
      </div>";

    }

    private function affItem() {
        $app = Slim::getInstance();
        $e=explode('/', $app->request->getResourceUri());
        $i=Item::where('id', '=', $e[2])->first();
        $u=User::select('Nom')->where('user_id', '=', $_SESSION['id'])->first();
        $root = $app->request->getRootUri();
        $urlListe = $app->urlFor('listes');
        $urlComm=null;
        if ($i->url != "") {
            $urlComm="<a class=\"right\" href='$i->url'>Lien commercial</a>";
        }
        $html="<div class=\"row\">
        <div class=\"col s12 m8 offset-m2 l6 offset-l3\">
        <div class=\"card grey lighten-2\">
          <div class=\"row valign-wrapper\">
            <div class=\"col s8\">
              <span class=\"black-text\">
                <h4>$i->nom</h4>
                $i->descr;
                <h5>Prix : $i->tarif €</h5>
              </span><br>";

        if ($i->reservation == null) {
            $res=$app->urlFor('res', array('id' => $i->id));
            $html.="<form method=\"post\" action=\"$res\">
                <div class=\"input-field col s8\">
                  <input value=\"$u->Nom\" name='resname' id=\"res\" type=\"text\" class=\"validate\">
                  <label class=\"active\" for=\"res\">Nom</label>
                </div>
                <div class=\"input-field col s8\">
                  <input value=\"$u->msg\" name='resmsg' id=\"msg\" type=\"text\" class=\"validate\">
                  <label class=\"active\" for=\"res\">Message</label>
                </div>
                <div class=\"col s1\">
                    <button class=\"btn-floating tooltipped waves-effect waves-light green\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Réserver\"><i class=\"material-icons\">done</i></button>
                </div>
              </form>";
        } else {
            $html.="Réserver";
        }
            $html.="</div>
            <div class=\"col s4\">
              <img src=\"$root/img/$i->img\" alt=\"\" class=\"responsive-img\"> <!-- notice the \"circle\" class -->
            </div>
          </div>
            <div class=\"card-action\">
              <a href=\"$urlListe\">Retour a la liste</a>
              $urlComm
            </div>
        </div>
      </div>
      </div>";
        return $html;
    }

    private function affListeToken() {
        $app=Slim::getInstance();
        $e=explode('/', $app->request->getResourceUri());
        $l=Liste::where('token', '=', $e[2])->first();
        $r="<ul class=\"collapsible\" data-collapsible=\"accordion\">
                <li>
                  <div class=\"collapsible-header grey lighten-2 active\">$l->titre</div>
                  <div class=\"collapsible-body grey lighten-3\">
                    <span>
                        <table class='highlight centered'>
                            <thead>
                              <tr>
                                  <th>Item Name</th>
                                  <th>Item Image</th>
                              </tr>
                            </thead>
                    
                            <tbody>";
        $lI = \mywishlist\models\Item::select('id', 'nom', 'img')->where('liste_id', '=', $l->no)->get();
        $image = $app->request->getRootUri();

        foreach ($lI as $value) {
            $urlItem = $app->urlFor('affItem', array('id' => $value->id));
            $r=$r."<tr>
                    <td><a href='$urlItem' class='black-text'>$value->nom</a></td>
                    <td><img width=\"150\" height=\"150\" class=\"responsive-img\" src=\"$image/img/$value->img\"></td>
                   </tr>";
        }
                            $r.="</tbody>
                          </table>
                    </span>
                  </div>
                </li>
              </ul>";
                              return $r;
    }
}
