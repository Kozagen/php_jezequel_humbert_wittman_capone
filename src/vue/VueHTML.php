<?php
namespace mywishlist\vue;

use Slim\Slim;

class VueHTML {

	public static function getHeaders() {
        $app = Slim::getInstance();
        $root = $app->request->getRootUri();
		return
		"<!DOCTYPE html>
		<html style=\"font-family: 'Comfortaa', cursive !important;\">
			<head>
				<meta charset=\"utf-8\" />
        		<title>My Wishlist</title>
        		
         		<!--Import Google Icon Font-->
        		<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
        		<!--Import materialize.css-->
        		<link type=\"text/css\" rel=\"stylesheet\" href=\"$root/css/materialize.css\"  media=\"screen,projection\"/>
                <link href=\"https://fonts.googleapis.com/css?family=Comfortaa\" rel=\"stylesheet\">
        		<!--Let browser know website is optimized for mobile-->
        		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
    		</head>
    	<body class='' background='$root/img/fond.jpg'>";
	}

	public static function getFooter() {
        $app = Slim::getInstance();
        $root = $app->request->getRootUri();
		return
		"<!--Import jQuery before materialize.js-->
        <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>
        <script type=\"text/javascript\" src=\"$root/js/materialize.js\"></script>
		</body>
	</html>";
	}

	public static function getMenu() {
        $app = \Slim\Slim::getInstance();
        $r_connexion = $app->urlFor("connexion");
        $r_inscription = $app->urlFor("inscription");
        $r_accueil = $app->urlFor("accueil");
        $r_listes = $app->urlFor("listes");
        $r_listesP = $app->urlFor("listesPub");
        $r_deco = $app->urlFor("deco");
        $r_pro = $app->urlFor("profil");

        $r=
		"<div class='navbar-fixed'>
            <nav>
                <div class=\"nav-wrapper grey lighten-2\">
                    <a href=\"$r_accueil\" class=\"brand-logo center indigo-text darken-1\">My Wishlist</a>
                    <a href=\"#\" data-activates=\"mobile\" class=\"button-collapse\"><i class=\"material-icons\">menu</i></a>";
        if (!isset($_SESSION['id'])) {
            $r = $r."
                    
                    <ul class=\"right hide-on-med-and-down\">
                        <li><a class=\"black-text\" href=\"$r_inscription\">Inscription</a></li>
                        <li><a class=\"waves-effect waves-light btn indigo darken-1 hoverable\" href=\"$r_connexion\">Connexion</a></li>
                    </ul>
                    <ul class='side-nav' id='mobile'>
                        <li><a href=\"$r_inscription\">Inscription</a></li>
                        <li><a href=\"$r_connexion\">Connexion</a></li>
                    </ul>";
        } else {
            $r = $r."<ul class=\"left hide-on-med-and-down\">
                        <li><a class=\"waves-effect waves-light btn indigo darken-1 hoverable\" href=\"$r_listes\">Wishlist</a></li>
                        
                    </ul>
                    <ul  class=\"right hide-on-med-and-down\">
                        <li><a class=\"black-text\" href=\"$r_pro\">Profil</a></li>
                        <li><a class=\"waves-effect waves-light btn indigo darken-1 hoverable\" href=\"$r_deco\">Deconnexion</a></li>
                    </ul>
                    <ul class='side-nav' id='mobile'>
                        <li><a href=\"$r_listes\">Wishlist</a></li>
                        <li><a href=\"$r_pro\">Profil</a></li>
                        <li><a href=\"$r_deco\">Deconnexion</a></li>
                    </ul>";
        }
                $r=$r."<a class=\"waves-effect waves-light btn indigo darken-1 hoverable\" href=\"$r_listesP\">Wishlist publique</a>
            </nav>
        </div>";
        return $r;
	}
}