<?php

namespace mywishlist\vue;

use Slim\Slim;

class VueParticipant{
	
	//attribut qui correspond au tableau des items et listes à afficher
	protected $tableauObjetAfficher;
	
	//constructeur qui prend en paramètre le tableau
	function __construct(/*$tableau*/) {
		//$tableauObjetAfficher=$tableau;
    }
	
	//renvoie un fragment html correspondant à l'affichage des listes de souhaits 
	//et des items qu'elles contiennent
	public function listeSouhaits(){
	    $app = Slim::getInstance();
	    $creerL = $app->urlFor('newListe');
	    $nL = \mywishlist\models\Liste::select('no', 'titre', 'description', 'msg')->where('user_id', '=', $_SESSION['id'])->get();
	$r="<ul class=\"collapsible popout\" data-collapsible=\"accordion\">";
	foreach ($nL as $value) {
        $url = $app->urlFor('modListe', array('id' => $value->no));
        $supp = $app->urlFor('suppListe', array('id' => $value->no));
        $r = $r . "<li>
      <div class=\"collapsible-header grey lighten-2 hoverable\">$value->titre</div>
      <div class=\"collapsible-body grey lighten-3\">
      <span>
        <table class=\"highlight centered\">
        <thead>
        <tr>";

        $r .= "<a class=\"btn-floating tooltipped light-blue darken-3\" 
        	        style='margin-left:10px;'
                    data-delay=\"50\" 
                    data-position='right'
                    data-tooltip=\"Edit\"
                    href='$url'>
                    <i class=\"material-icons\">edit</i>
            </a>
            <a class=\"btn-floating tooltipped black\" 
                style='margin-left:10px;'
                data-position=\"right\" 
                data-delay=\"50\" 
                data-tooltip=\"Supprimer la liste\" 
                href='$supp'>
                <i class=\"material-icons\">clear</i>
            </a><br><br>";


        if ($value->description != "") {
            $r .= "Description: $value->description
            <br>";
        }
        if ($value->msg != "") {
            $r .= "Message: $value->msg";
        }
        $r .= "</tr>
          <tr>
              <th>Name</th>
              <th>Image</th>
          </tr>
        </thead>

        <tbody>";
        $lI = \mywishlist\models\Item::select('id', 'nom', 'img')->where('liste_id', '=', $value->no)->get();
        $image = $app->request->getRootUri();

        foreach ($lI as $value) {
            $urlItem = $app->urlFor('affItem', array('id' => $value->id));
            $r = $r . "<tr>
                    <td><a href='$urlItem' class='black-text'>$value->nom</a></td>
                    <td><img width=\"150\" height=\"150\" class=\"responsive-img\" src=\"$image/img/$value->img\"></td>
                   </tr>";
        }
        $r = $r . "</tbody>
      </table>";

        $r = $r . "</li>";
    }
	    $r = $r."<div class='fixed-action-btn horizontal'>
                <a class=\"btn-floating tooltipped btn-large green\" 
                    data-delay=\"50\" 
                    data-position='left'
                    data-tooltip=\"Nouvelle liste\"
                    href=\"$creerL\">
                    <i class=\"material-icons\">add</i>
                </a>
            </div>";

      return $r;
	}

    public function listePublique(){
        $app = Slim::getInstance();

        $nL = \mywishlist\models\Liste::select('no', 'titre', 'description', 'msg')->where('public', '=', 1)->get();
        $r="<ul class=\"collapsible popout\" data-collapsible=\"accordion\">";
        foreach ($nL as $value) {
            $r=$r."<li>
      <div class=\"collapsible-header grey lighten-2 hoverable\">$value->titre</div>
      <div class=\"collapsible-body grey lighten-3\">
      <span>
        <table class=\"highlight centered\">
        <thead>
        <tr>";
            if ($value->description != "") {
                $r .="Description: $value->description
            <br>";
            }
            if ($value->msg != "") {
                $r .= "Message: $value->msg";
            }
            $r.="</tr>
          <tr>
              <th>Name</th>
              <th>Image</th>
          </tr>
        </thead>

        <tbody>";
            $lI = \mywishlist\models\Item::select('id', 'nom', 'img')->where('liste_id', '=', $value->no)->get();
            $image = $app->request->getRootUri();

            foreach ($lI as $value) {
                $urlItem = $app->urlFor('affItem', array('id' => $value->id));
                $r=$r."<tr>
                    <td><a href='$urlItem' class='black-text'>$value->nom</a></td>
                    <td><img width=\"150\" height=\"150\" class=\"responsive-img\" src=\"$image/img/$value->img\"></td>
                   </tr>";
            }
            $r=$r."</tbody>
      </table>";

            $r = $r."</li>";
        }
        return $r;
    }
}