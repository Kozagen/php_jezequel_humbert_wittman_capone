<?php
//include 'header.php';
require 'vendor/autoload.php';
session_start();
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim;

$app->get('/', function(){
	(new mywishlist\controleur\ControleurUser())->index();
})->name("accueil");

$app->get('/listes', function(){
	(new mywishlist\controleur\ControleurUser())->listes();
})->name("listes");

$app->get('/listes-publique', function(){
    print (new mywishlist\vue\VueNavigation())->render('listesPub');
})->name("listesPub");

$app->get('/connexion', function(){
    (new mywishlist\controleur\ControleurUser())->connexion();
})->name("connexion");

$app->post('/connexion', function(){
    (new mywishlist\controleur\ControleurUser())->connexion();
})->name("connexionP");

$app->post('/inscription', function(){
    (new mywishlist\controleur\ControleurUser())->inscription();
})->name("inscriptionP");

$app->get('/inscription', function(){
	(new mywishlist\controleur\ControleurUser())->inscription();
})->name("inscription");

$app->get('/deconnexion', function(){
    (new mywishlist\controleur\ControleurUser())->deconnexion();
})->name("deco");

$app->get('/creerListe', function(){
    (new mywishlist\controleur\ControleurListe())->creerListe();
})->name("newListe");

$app->get('/modifierListe/:id', function($id){
    if (isset($_SESSION['id'])) {
        $l=\mywishlist\models\Liste::where('no', '=', $id)->first();
        if ($l != null && $l->user_id == $_SESSION['id']) {
            print (new mywishlist\vue\VueNavigation())->render("modifL");
        }
    }
})->name("modListe");

$app->get('/suppListe/:id', function($id){
    \mywishlist\controleur\ControleurListe::supprimerListe($id);
    $app=\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('listes'));
})->name("suppListe");

$app->post('/modifierListe/:id', function($id){
    if (isset($_SESSION['id'])) {
        $l=\mywishlist\models\Liste::where('no', '=', $id)->first();
        if ($l != null && $l->user_id==$_SESSION['id']) {
            \mywishlist\controleur\ControleurListe::modifierListe($id);
        }
    }
})->name("modListePost");

$app->get('/profil', function(){
    print (new mywishlist\vue\VueNavigation())->render("profile");
})->name("profil");

$app->post('/modifProfil', function(){
    print (new mywishlist\controleur\ControleurUser())->modifProfil();
})->name("mProfil");

$app->get('/ajouterItem/:id', function(){
    print (new \mywishlist\vue\VueNavigation())->render("modifI");
})->name("addItem");

$app->get('/modifierItem/:idL/:id', function(){
    print (new \mywishlist\vue\VueNavigation())->render("modifI");
})->name("modItem");

$app->post('/modifierItem/:idL/:id', function($idL, $id){
    $n = \mywishlist\models\Item::where('id', '=', $id)->first();
    $n->nom = $_POST['title'];
    $n->descr = $_POST['dsc'];
    $n->tarif = $_POST['prix'];
    if ($_POST['url'] != "") {
        $n->url = $_POST['url'];
    }
    if ($_FILES['file'] != "") {
        $n->img = $_FILES['file']['name'];
        $app=\Slim\Slim::getInstance();
        $image = $_SERVER['SERVER_NAME'].$app->request->getRootUri();
        $nom= "$image".'/img/'.$_FILES['file']['name'];
        copy($_FILES['file']['name'],$nom);
       
        
    }
    $n->update();
    $app=\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('modListe', array( 'id' => $idL)));
})->name("modItemP");

$app->post('/ajouterItem/liste-:id', function($id){
    $n = new mywishlist\models\Item();
    $n->liste_id = $id;
    $n->nom = $_POST['title'];
    $n->descr = $_POST['dsc'];
    $n->tarif = $_POST['prix'];
    if ($_POST['url'] != "") {
        $n->url = $_POST['url'];
    }


    if ($_FILES['file'] != "") {
    $n->img = $_FILES['file']['name'];
    $app=\Slim\Slim::getInstance();
    $image = $app->request->getRootUri();
    $nom = $image."img/".$_FILES['file']['name'];
    copy($_FILES['file']['tmp_name'],$nom);

    }
    $n->save();
    $app=\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('modListe', array( 'id' => $id)));
})->name("addItemP");

$app->get('/supprimerItem/:id', function($id){
    $i=\mywishlist\models\Item::Select('liste_id')->where('id', '=', $id)->first();
    \mywishlist\controleur\ControleurItem::supprimeritem($id);
    $app=\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('modListe', array( 'id' => $i->liste_id)));
})->name("suppItem");

$app->get('/item/:id', function(){
    print (new \mywishlist\vue\VueNavigation())->render("affItem");
})->name("affItem");

$app->post('/reserver/:id', function($id){
    $i=\mywishlist\models\Item::where('id', '=', $id)->first();
    $i->reservation=$_POST['resname'];
    $i->msg = $_POST['resmsg'];
    $i->update();
    $app=\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('affItem', array( 'id' => $id)));
})->name("res");

$app->get('/listeUrl/:token', function(){
    print (new \mywishlist\vue\VueNavigation())->render("affListeToken");
})->name("listeUrl");

$app->get('/addUrl/:id', function($id){
    $li=\mywishlist\models\Liste::where('no', '=', $id)->first();
    $li->token=md5($li->titre);
    $li->update();
    $app=\Slim\Slim::getInstance();
    $app->redirect($app->urlFor('modListe', array( 'id' => $id)));
})->name("addUrl");

$app->get('/supprimerUser', function(){
    if (isset($_SESSION['id'])) {
        $li=\mywishlist\models\Liste::select('no')->where('user_id', '=', $_SESSION['id'])->get();
        foreach ($li as $value) {
            \mywishlist\controleur\ControleurListe::supprimerliste($value->no);
        }
        \mywishlist\controleur\ControleurUser::supprimerUser();
        $app=\Slim\Slim::getInstance();
        $app->redirect($app->urlFor('accueil'));
    }
} ) ->name("suppUser");
$app -> run();